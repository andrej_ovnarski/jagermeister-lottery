<?php

if (isset($_POST['loginBtn'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    require_once '../classes/LoginController.php';
    $admin = new LoginController($username, $password);
    $admin->loginAdmin();
}
