<?php
session_start();
if (isset($_SESSION['username'])) {
    header('location: /Dashboard/dashboard.php');
    die();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
</head>

<body>
    <nav class="d-flex justify-content-between align-items-center" style="background-color: white; ">
        <a href="#"><img src="../pictures/326-3265136_jagermeister-png-transparent-png.png" alt="" style="width: 200px;" class="my-3 ml-4"></a>
    </nav>
    <div class="bg">
        <div class="row align-items-center h-100 w-100 justify-content-center">
            <div class="col-2 bg-light rounded">
                <?php if (isset($_GET['error']))
                    echo '<div class="alert alert-danger my-3 mx-auto w-auto" role="alert">' . $_GET['error'] . '</div>'
                ?>
                <img src="../pictures/326-3265136_jagermeister-png-transparent-png.png" alt="" style="width: 150px;" class="my-3 mx-auto d-block">
                <form action="admin.php" method="post">
                    <label for="username" class="mt-3">Enter username:</label>
                    <input class="form-control mb-3" type="text" aria-label="default input example" name="username" id="username">
                    <label for="username">Enter password:</label>
                    <input class="form-control" type="password" aria-label="default input example" name="password" id="password">
                    <button class="btn btn-outline-success my-3 mx-auto d-block" type="submit" id="loginBtn" name="loginBtn">Log in</button>
                </form>

            </div>
        </div>
    </div>




    <script src="../javascript/jquery-3.6.0.js"></script>
    <script src="../javascript/admin.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>