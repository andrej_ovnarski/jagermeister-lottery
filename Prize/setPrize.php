<?php
require_once '../classes/Prize.php';
$prizeId = $_GET['prize'];
$winnerId = $_GET['id'];
$prize = new Prize();

$prize->givePrize($prizeId, $winnerId);
$amount = $prize->getAmount($prizeId);
$newAmount = $amount - 1;
$prize->setAmount($newAmount, $prizeId);
header('location: ../Dashboard/dashboard.php?success= Prize is given');
die();
