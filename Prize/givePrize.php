<?php
session_start();
require_once '../classes/AdminPrizes.php';
require_once '../classes/Prize.php';
$id = $_GET['id'];
$prize = new AdminPrizes();
$isApproved = 1;
$listPrize = new Prize();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
</head>

<body>
    <nav class="d-flex justify-content-between align-items-center" style="background-color: white; ">
        <a href="#"><img src="../pictures/326-3265136_jagermeister-png-transparent-png.png" alt="" style="width: 200px;" class="my-3 ml-4"></a>
        <a href="logout.php" style="color: goldenrod; text-decoration: none;" class="mx-3">Logout</a>
    </nav>
    <div class="bg">
        <div class="container h-100">
            <div class="row align-items-center h-auto mb-5 w-100 text-center justify-content-center flex-column h-100">
                <div class="col rounded flex-wrap justify-content-center align-itmes-center w-auto h-auto mt-5" style="background-color: white;">
                    <form action="setPrize.php" method="get">
                        <h2 class="mt-2">Give Prize</h2>
                        <?php $prize->winnerPrize($isApproved, $id); ?>
                        <select class="form-select" id="inputGroupSelect01" name="prize">
                            <?php $listPrize->selectPrize(); ?>
                        </select>
                        <button class="btn btn-success mt-5 mb-2" id="prizeBtn" type="submit">Finish</button>
                        <input type="hidden" name="id" value="<?php echo $id ?>">

                    </form>
                </div>
            </div>
        </div>
    </div>



    <script src="../javascript/jquery-3.6.0.js"></script>
    <script src="../javascript/main.js"></script>
    <script src="../javascript/buttons.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>