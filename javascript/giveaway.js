$(function () {  
    $('#finishBtn').on('click', function () {
        let progress = `
            <p class="mx-3">Loading information</p>
            <div class="d-flex justify-content-center my-3">
                <div class="spinner-border" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </div>
        `
        
        $('#progress').html(progress)
        $('#progress').css('display', 'block')
        $('#infoText').css('display', 'none')
        $('#finalForm').css('display', 'none')
        
    })  
})
