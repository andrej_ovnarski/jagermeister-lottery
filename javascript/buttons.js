$(function () {
    $("#allCards").css("display","flex")
    $("#declinedCards").css("display","none")
    $("#approvedCards").css("display","none")

    $('#approved').click(function () { 
        $("#allCards").css("display","none")
        $("#declinedCards").css("display","none")
        $("#approvedCards").css("display","flex")
        
    });
    $('#all').click(function () { 
        $("#allCards").css("display","flex")
        $("#declinedCards").css("display","none")
        $("#approvedCards").css("display","none")
        
    });
    $('#declined').click(function () { 
        $("#allCards").css("display","none")
        $("#declinedCards").css("display","flex")
        $("#approvedCards").css("display","none")
        
    });
})