$(function () {

    $.ajax({
        type: "post",
        url: "cards.php",
        dataType: "json",
        success: function (data) {
            data.forEach(element => {
                $('#allCards').append(`
                <form method="post">
                <div class="card my-3 mx-3" style="width: 16rem;">
                <img src="/images/${element.ticket}" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text"> ${element.email} </p>
                    <div>
                        <a href="../CRUD/approve.php?id=${element.id}" class="btn btn-primary" type="submit" name="approvedBtn" id="approvedBtn">Approve</a>
                        <a href="../CRUD/decline.php?id=${element.id}" class="btn btn-danger" type="submit" name="declinedBtn">Decline</a>
                    </div>
                </div>
                </div>
                </form>`,
                );
            });
            
        }
    }),
    $.ajax({
        type: "post",
        url: "approvedCards.php",
        dataType: "json",
        success: function (data) {
            data.forEach(element => {
                console.log(element.prize_id)
                if(element.prize_id === null){
                    $('#approvedCards').append(`  
                <div class="card my-3 mx-3" style="width: 16rem;">
                <img src="/images/${element.ticket}" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text"> ${element.email} </p>
                    <div>
                        <a href="../Prize/givePrize.php?id=${element.id}" class="btn btn-success" id="prizeBtn" type="submit">Give Prize</a>
                    </div>
                </form>
                </div>`
                
                );
                }else {
                    $('#approvedCards').append(`  
                    <div class="card my-3 mx-3" style="width: 16rem;">
                    <img src="/images/${element.ticket}" class="card-img-top" alt="...">
                    <div class="card-body">
                            <p class="card-text"> ${element.email} </p>
                            <p class="card-text">Awarded</p>
                        <form method="post">
                            <a href="../CRUD/delete.php?id=${element.id}&name=${element.ticket}" class="btn btn-danger" id="deleteBtn" type="submit" name="deleteBtn">Delete</a>
                        </form>
                    </div>`)
                }
                
        });
    }
    }),
    $.ajax({
        type: "post",
        url: "declinedCards.php",
        dataType: "json",
        success: function (data) {
            data.forEach(element => {
                $('#declinedCards').append(`
                <div class="card my-3 mx-3" style="width: 16rem;">
                <img src="/images/${element.ticket}" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text"> ${element.email} </p>
                    <form method="post">
                        <a href="../CRUD/delete.php?id=${element.id}&name=${element.ticket}" class="btn btn-danger" id="deleteBtn" type="submit" name="deleteBtn">Delete</a>
                    </form>
                </div>
                </div>`
                );
            });
            
        }
    })
});

