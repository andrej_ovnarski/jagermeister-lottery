<?php
session_start();
require_once './classes/Prize.php';
$prize = new Prize();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lottery</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="./style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
</head>

<body>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="exampleModalLabel">Contact Information</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="mx-2" id="progress"></div>
                <div class="mx-2" id="finalForm"></div>
                <div class="mx-2" id="infoText">
                    <form action="giveaway.php" method="post" enctype="multipart/form-data">
                        <label for="email" class=" mb-0">Enter E-mail:</label>
                        <input type="email" id="email" class=" mb-4 form-control" name="email" autocomplete="">
                        <p>Upload the image of the reciept</p>
                        <input type="file" class="mb-3 d-block" name="receipt" id="receipt">
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-outline-warning mb-3" id="closeBtn" data-bs-dismiss="modal" type="button">Close</button>
                            <button type="submit" class="btn btn-outline-success mb-3 ms-3" id="finishBtn" name="finishBtn">Finish</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <nav class="d-flex justify-content-between align-items-center" style="background-color: white; ">
        <a href="#"><img src="./pictures/326-3265136_jagermeister-png-transparent-png.png" alt="" style="width: 200px;" class="my-3 ml-4"></a>
        <div>
            <a href=""><i class="fas fa-share-alt mx-3 my-4" style="color: goldenrod; font-size: 20px;"></i></a>
            <a href="/Login/loginAdmin.php" style="color: goldenrod; text-decoration: none;" class="mx-3">Admin</a>
        </div>
    </nav>
    <div class=" bg">
        <div class="row align-items-center h-100 w-100 text-center justify-content-center">
            <div class="col-5 rounded" style="background-color: white;">
                <?php if (isset($_GET['error'])) : ?>
                    <div class="alert alert-danger my-3 mx-auto w-auto" role="alert">
                        <?php echo $_GET['error'] ?>
                    </div>
                <?php elseif (isset($_GET['success'])) : ?>
                    <div class="alert alert-success my-3 mx-auto w-auto" role="alert">
                        <?php echo $_GET['success'] ?>
                    </div>
                <?php endif; ?>
                <img src="./pictures/326-3265136_jagermeister-png-transparent-png.png" alt="" style="width: 300px;" class="my-3">
                <div><?php $prize->getPrize(); ?></div>
                <button class="btn btn-warning d-inline-block my-4" id="giveawayBtn" data-bs-toggle="modal" data-bs-target="#exampleModal">Enter Giveaway</button>
            </div>
        </div>
    </div>












    <script src="./javascript/jquery-3.6.0.js"></script>
    <script src="./javascript/giveaway.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

</html>