<?php
require_once 'Database.php';

class AdminPrizes extends Database
{
    public function winners()
    {
        $sql = 'SELECT * FROM winners WHERE approved is NULL ';
        $stmt = $this->connect()->query($sql);
        $row = $stmt->fetchAll();
        echo json_encode($row);
    }

    public function approvedWinners($approved)
    {
        $sql = 'SELECT * FROM winners WHERE approved = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$approved]);
        $row = $stmt->fetchAll();
        echo json_encode($row);
    }

    public function declinedWinners($declined)
    {
        $sql = 'SELECT * FROM winners WHERE approved = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$declined]);
        $row = $stmt->fetchAll();
        echo json_encode($row);
    }

    public function approve($approve, $id)
    {
        $sql = 'UPDATE winners SET approved = ? WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$approve, $id]);
    }
    public function decline($approve, $id)
    {
        $sql = 'UPDATE winners SET approved = ? WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$approve, $id]);
    }

    public function winnerPrize($approve, $id)
    {
        $sql = 'SELECT * FROM winners WHERE approved = ? AND id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$approve, $id]);
        while ($row = $stmt->fetch()) {
            echo '<img src="../images/' . $row['ticket'] . '" alt="" class="mt-2 rounded">
                    <p class="mb-2">' . $row['email'] . '</p>';
        }
    }

    public function deleteUser($id)
    {
        $sql = 'DELETE FROM winners WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$id]);
    }

    public function updatePrize($prizeId, $id)
    {
        $sql = 'UPDATE winners SET prize_id = ? WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$prizeId, $id]);
    }
}
