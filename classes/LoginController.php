<?php

require_once 'Login.php';

class LoginController extends Login
{

    private $username;
    private $password;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }


    public function loginAdmin()
    {
        if ($this->emptyInput() == false) {
            echo json_encode(["msg" => "Empty input", "status" => 500]);
            header('location: loginadmin.php?error=Please fill all the inputs');
            die();
        }

        $this->getAdmin($this->username, $this->password);
    }

    public function emptyInput()
    {
        $result = true;

        if (empty($this->username) || empty($this->password)) {
            $result = false;
        }
        return $result;
    }
}
