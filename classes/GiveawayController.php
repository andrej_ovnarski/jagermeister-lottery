<?php
require_once 'GiveawayRegister.php';
class GiveawayController extends GiveawayRegister
{
    private $email;
    private $receipt;

    public function __construct($email, $receipt)
    {
        $this->email = $email;
        $this->receipt = $receipt;
    }

    public function checkUser()
    {
        if ($this->emptyInput() == false) {
            echo json_encode(["msg" => "Empty input", "status" => 500]);
            header('location: index.php?error=Please fill all the inputs');
            die();
        }
        if ($this->invalidEmail() == false) {
            echo json_encode(["msg" => "Invalid email", "status" => 500]);
            header('location: index.php?error=Invalid email');
            die();
        }
        if ($this->checkTicket() == false) {
            echo json_encode(["msg" => "Invalid image", "status" => 500]);
            header('location: index.php?error=Please insert image');
            die();
        }
        if ($this->checkedEmail() == false) {
            echo json_encode(["msg" => "Email taken", "status" => 500]);
            header('location: index.php?error=This email is taken');
            die();
        }

        $this->registerUser($this->email, $this->receipt['name']);
    }

    public function emptyInput()
    {
        $result = true;

        if (empty($this->email) || empty($this->receipt)) {
            $result = false;
        }
        return $result;
    }

    public function invalidEmail()
    {
        $result = true;

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $result = false;
        }

        return $result;
    }

    public function checkTicket()
    {
        $result = true;

        if ($this->receipt['type'] != 'image/jpeg') {
            $result = false;
        }

        return $result;
    }
    public function checkedEmail()
    {
        $result = true;
        if ($this->checkEmail($this->email) == false) {
            $result = false;
        }

        return $result;
    }
}
