<?php

require_once 'Database.php';
class GiveawayRegister extends Database
{
    public function registerUser($email, $receipt)
    {
        $sql = 'INSERT INTO winners (email, ticket) VALUES (? , ?)';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$email, $receipt]);
    }
    public function checkEmail($email)
    {
        $sql = 'SELECT * FROM winners WHERE email = ?';
        $stmt = $this->connect()->prepare($sql);

        if (!$stmt->execute([$email])) {
            $stmt = null;
            header('location: index.php?stmt=failed');
            die();
        }
        $resultCheck = true;
        if ($stmt->rowCount() > 0) {
            $resultCheck = false;
        } else {
            $resultCheck = true;
        }
        return $resultCheck;
    }
}
