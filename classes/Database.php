<?php


abstract class Database
{
    private $host = 'localhost';
    private $dbname = 'lottery';
    private $user = 'root';
    private $password = '';


    protected function connect()
    {
        try {
            $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
            $pdo = new PDO($dsn, $this->user, $this->password);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            return $pdo;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
}
