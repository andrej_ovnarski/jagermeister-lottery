<?php

require_once 'Database.php';

class Login extends Database
{
    public function getAdmin($username, $password)
    {
        $sql = 'SELECT `password` FROM `admin` WHERE username = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$username]);

        if ($stmt->rowCount() === 0) {
            $stmt = null;
            header('location: loginAdmin.php?error=User not found');
            die();
        }
        $row = $stmt->fetchAll();
        if ($row[0]['password'] === md5($password)) {
            session_start();
            $_SESSION['username'] = $username;
            header('location: /Dashboard/dashboard.php');
            die();
        } else {
            header('location: loginAdmin.php?error=Wrong password');
            die();
        }
    }
}
