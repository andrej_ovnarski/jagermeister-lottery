<?php

require_once 'Database.php';

class Prize extends Database
{
    public function getPrize()
    {
        $sql = 'SELECT * FROM prizes';
        $stmt = $this->connect()->query($sql);
        while ($row = $stmt->fetch()) {
            if ($row['amount'] > 0) {
                echo '
                <div class="d-inline-block" style="width: 12rem;">
                    <img src="./pictures/' . $row['prize'] . '" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text" id="case1"> x' . $row['amount'] . '</p>
                    </div>
                </div>';
            } else {
                echo '
                <div class="d-inline-block" style="width: 12rem;">
                    <img src="./pictures/' . $row['prize'] . '" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text" id="case1"> Out of stock </p>
                    </div>
                </div>';
            }
        }
    }
    public function selectPrize()
    {
        $sql = 'SELECT * FROM prizes';
        $stmt = $this->connect()->query($sql);
        while ($row = $stmt->fetch()) {
            if ($row['amount'] != 0) {
                echo '
                <option value="' . $row['id'] . '">' . $row['name'] . ' </option>
                ';
            } else {
                echo '
                <option value="' . $row['id'] . '" disabled>' . $row['name'] . ' is out of stock </option>
                ';
            }
        }
    }

    public function getAmount($id)
    {
        $sql = 'SELECT amount FROM prizes WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$id]);
        $row = $stmt->fetch();
        return ($row['amount']);
    }


    public function setAmount(int $amount, $id)
    {
        $sql = 'UPDATE prizes SET amount = ? WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $row = $stmt->execute([$amount, $id]);
        print_r($row);
    }

    public function givePrize($prizeId, $id)
    {
        $sql = 'UPDATE winners SET prize_id = ? WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$prizeId, $id]);
    }
}
