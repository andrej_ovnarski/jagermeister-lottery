<?php
require_once '../classes/AdminPrizes.php';
if (!isset($_POST['deleteBtn'])) {
    $id = $_GET['id'];
    $img = $_GET['name'];
    $deleteUser = new AdminPrizes();
    $deleteUser->deleteUser($id);
    $path = "images/$img";
    if (!unlink($path)) {
        echo "fail";
    } else {
        echo "success";
    }
    header('location: /Dashboard/dashboard.php');
    die();
}
