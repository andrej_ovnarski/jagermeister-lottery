<?php
require_once '../classes/AdminPrizes.php';
$prize = new AdminPrizes();
$isApproved = 1;
if (!isset($_POST['approvedBtn'])) {
    $id = $_GET['id'];
    $prize->approve($isApproved, $id);
    header('location: /Dashboard/dashboard.php');
    die();
}
