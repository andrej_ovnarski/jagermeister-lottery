<?php


require_once '../classes/AdminPrizes.php';
$prize = new AdminPrizes();
$isDeclined = 0;
if (!isset($_POST['declinedBtn'])) {
    $id = $_GET['id'];
    $prize->decline($isDeclined, $id);
    header('location: /Dashboard/dashboard.php');
    die();
}
