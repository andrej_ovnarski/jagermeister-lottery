<?php

if (isset($_POST['finishBtn'])) {
    $email = $_POST['email'];
    $receipt = $_FILES['receipt'];
    $nameImg = $receipt['name'];

    require_once './classes/GiveawayController.php';
    $winner = new GiveawayController($email, $receipt);
    $winner->checkUser();
    move_uploaded_file($_FILES['receipt']['tmp_name'], "images/$nameImg");
    echo json_encode(["msg" => "Thanks for participating"]);
    header('location: index.php?success=Thanks for participating');
    die();
}
