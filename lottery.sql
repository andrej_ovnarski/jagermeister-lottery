-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2022 at 06:17 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lottery`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500');

-- --------------------------------------------------------

--
-- Table structure for table `prizes`
--

CREATE TABLE `prizes` (
  `id` int(11) NOT NULL,
  `prize` varchar(266) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prizes`
--

INSERT INTO `prizes` (`id`, `prize`, `amount`, `name`) VALUES
(1, 'whiteCap.jpg', 1, 'White Cap'),
(2, 'greyCap.jpg', 5, 'Grey Cap'),
(3, 'blackCap.jpg', 0, 'Black Cap'),
(4, 'blackMask.jpg', 6, 'Black Mask'),
(5, 'colorMask.jpg', 6, 'Colorful Mask'),
(6, 'phonecase.jpg', 2, 'Grey Mask');

-- --------------------------------------------------------

--
-- Table structure for table `winners`
--

CREATE TABLE `winners` (
  `id` int(11) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `ticket` varchar(64) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `prize_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `winners`
--

INSERT INTO `winners` (`id`, `email`, `ticket`, `approved`, `prize_id`) VALUES
(5, 'john@example.com', 'table-chairs_7OPWOSQFXS.jpg', 1, NULL),
(16, 'logan@example.com', 'computer-keyboard_BTG0UZK1NX.jpg', NULL, NULL),
(17, 'jackie@example.com', '26ee5-learnprogramming.jpg', 0, NULL),
(21, 'anna@example.com', 'home.jpg', NULL, NULL),
(24, 'kingma@msn.com', 'david-kovalenko-G85VuTpw6jg-unsplash.jpg', NULL, NULL),
(25, 'chinthaka@comcast.net', 'davisuko-5E5N49RWtbA-unsplash.jpg', 0, NULL),
(26, 'mcast@msn.com', 'raimond-klavins-uAk731NvaJo-unsplash.jpg', NULL, NULL),
(31, '123@gmail.com', 'david-kovalenko-G85VuTpw6jg-unsplash.jpg', 0, NULL),
(32, 'ovnarskia@gmail.com', 'mike-dorner-sf_1ZDA1YFw-unsplash.jpg', NULL, NULL),
(33, '456@gmail.com', 'davisuko-5E5N49RWtbA-unsplash.jpg', 1, NULL),
(34, 'sarah@example.com', 'diego-ph-fIq0tET6llw-unsplash.jpg', 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prizes`
--
ALTER TABLE `prizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winners`
--
ALTER TABLE `winners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prize_id` (`prize_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prizes`
--
ALTER TABLE `prizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `winners`
--
ALTER TABLE `winners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `winners`
--
ALTER TABLE `winners`
  ADD CONSTRAINT `winners_ibfk_1` FOREIGN KEY (`prize_id`) REFERENCES `prizes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
